Name: dhcp_probe
Version: 1.3.0
Release: 3%{?dist}
Summary: Tool for discover DHCP and BootP servers
License: GPLv2+ and MIT
Group: System Environment/Daemons
Url: http://www.net.princeton.edu/software/dhcp_probe/
Source0: http://www.net.princeton.edu/software/dhcp_probe/%{name}-%{version}.tar.gz
Patch0: dhcp_probe-1.3.0-guignard-03_implicit_point_conv_bootp.c.patch
Patch1: dhcp_probe-1.3.0-guignard-04_linux_32_or_64bits.patch
Patch2: dhcp_probe-1.3.0-virta-01-pcap-loop.patch 
Patch3: dhcp_probe-1.3.0-virta-02-keep-pcap.patch 
Patch4: dhcp_probe-1.3.0-virta-03-drop-privs.patch
BuildRequires: libnet-devel libpcap-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
dhcp_probe attempts to discover DHCP and BootP servers on a directly-attached
Ethernet network. A network administrator can use this tool to locate 
unauthorized DHCP and BootP servers.

%prep
%setup -q
%patch0
%patch1
%patch2
%patch3
%patch4

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot} 
make DESTDIR=%{buildroot} INSTALL="install -p" CP="cp -p" install
install -p -m 644 -D extras/dhcp_probe.cf.sample %{buildroot}/%{_sysconfdir}/dhcp_probe.cf

%clean
rm -rf %{buildroot} 

%files
%defattr(-,root,root,-)
%doc INSTALL INSTALL.dhcp_probe README COPYING.GPL COPYING COPYING.LIB
%config(noreplace) %_sysconfdir/dhcp_probe.cf
%{_sbindir}/%{name}
%{_mandir}/man8/*
%{_mandir}/man5/*

%changelog
* Sun Jul 11 2010 Guillermo Gomez <ggomez@neotechgw.com> - 1.3.0-3
- Source0 url now use macros. Configuration file now preserve 
  source timestamp at install stage.

* Tue Jul 06 2010 Guillermo Gomez <ggomez@neotechgw.com> - 1.3.0-2
- Description typo corrected. Default  permissions corrected.

* Wed Mar 24 2010 Guillermo Gómez <ggomez@neotechgw.com> - 1.3.0-1
- First Fedora spec compliant version
